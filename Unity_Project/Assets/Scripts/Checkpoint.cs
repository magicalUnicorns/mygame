﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

	[SerializeField]
	string checkpointTag;

	void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.tag == "Player")
        {
        	GameObject platform;
            platform = GameObject.FindGameObjectWithTag(checkpointTag);
            PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
            Vector3 platformInfo = platform.transform.transform.position;
            playerController.setSpawningPosition(platformInfo);
       }
    }
}
